package balam.app.examen.model;

public class UserModel {
    private String edtName, edtApellido, edtEmail, edtPassword, edtNacimiento, avatar,token, latitud, longitud;

    public UserModel() {
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEdtPassword() {
        return edtPassword;
    }

    public void setEdtPassword(String edtPassword) {
        this.edtPassword = edtPassword;
    }

    public String getEdtName() {
        return edtName;
    }

    public void setEdtName(String edtName) {
        this.edtName = edtName;
    }

    public String getEdtApellido() {
        return edtApellido;
    }

    public void setEdtApellido(String edtApellido) {
        this.edtApellido = edtApellido;
    }

    public String getEdtEmail() {
        return edtEmail;
    }

    public void setEdtEmail(String edtEmail) {
        this.edtEmail = edtEmail;
    }

    public String getEdtNacimiento() {
        return edtNacimiento;
    }

    public void setEdtNacimiento(String edtNacimiento) {
        this.edtNacimiento = edtNacimiento;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
