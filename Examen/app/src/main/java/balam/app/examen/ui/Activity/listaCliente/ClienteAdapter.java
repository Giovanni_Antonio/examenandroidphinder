package balam.app.examen.ui.Activity.listaCliente;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import balam.app.examen.R;

public class ClienteAdapter extends FirestoreRecyclerAdapter<ClientePojo, ClienteAdapter.ViewHolder> {

    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    private Context context;
    public ClienteAdapter(@NonNull FirestoreRecyclerOptions<ClientePojo> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull ClientePojo model) {
        holder.textViewId.setText(model.getId());
        holder.textViewName.setText(model.getNombre());

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cliente,parent,false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewId;
        TextView textViewName;



        public ViewHolder(@NonNull View itemView){
            super(itemView);
            textViewId = itemView.findViewById(R.id.edt_nombre_cliente);
            textViewName = itemView.findViewById(R.id.edt_contacto);



        }

    }
}
