package balam.app.examen.ui.Activity.mainActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import balam.app.examen.R;
import balam.app.examen.tools.Decompressfile;
import balam.app.examen.ui.Activity.listaCliente.ListaCliente;
import balam.app.examen.ui.Activity.registro.RegistroAcivity;

public class MainActivity extends AppCompatActivity {
    TextView mTxtDisplay;
    WebView webView;
    Button buttonCliente, buttonNewCliente;
    Boolean descarga = false;

    SharedPreferences prefs;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 507;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonCliente = (Button) findViewById(R.id.button);
        buttonNewCliente = (Button) findViewById(R.id.button2);
        prefs = this.getSharedPreferences("pref", Context.MODE_PRIVATE);

        buttonCliente.setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, ListaCliente.class));

        });

        buttonNewCliente.setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this,RegistroAcivity.class));
        });
        webView = (WebView) findViewById(R.id.webView);
        checkWritePermission();

        String url = "https://dl.dropboxusercontent.com/s/5u21281sca8gj94/getFile.json?dl=0";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                response -> {
                    try {

                        if (Update("descarga")==false){

                        if (descarga == false){

                            SaveIntoSharedPreferences("descarga",true);
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject dato= jsonObject.getJSONObject("data");
                        String archivo = dato.getString("file");
                        webView.loadUrl(String.valueOf(archivo));
                        webView.setDownloadListener((url1, userAgent, contentDisposition, mimeType, contentLength) -> {
                            DownloadManager.Request request = new DownloadManager.Request(
                                    Uri.parse(url1));

                            request.setMimeType(mimeType);
                            String cookies = CookieManager.getInstance().getCookie(url1);
                            request.addRequestHeader("cookie", cookies);
                            request.addRequestHeader("User-Agent", userAgent);
                            request.setDescription("Downloading file...");
                            request.setTitle(URLUtil.guessFileName(url1, contentDisposition, mimeType));
                            request.allowScanningByMediaScanner();
                            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            //La carpeta de destino seria descargas del telefono
                            request.setDestinationInExternalPublicDir(
                                    Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(
                                            url1, contentDisposition, mimeType));
                            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                            dm.enqueue(request);
                            Toast.makeText(getApplicationContext(), "Downloading File",
                                    Toast.LENGTH_LONG).show();
                            webView.setVisibility(View.GONE);
                            descarga = true;
                        });

                        }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                },
                error -> Toast.makeText(MainActivity.this,error.toString(),Toast.LENGTH_LONG).show()){

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void checkWritePermission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            // No action!.
        }else{
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);
            return;
        }
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_CODE_ASK_PERMISSIONS:
                if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    // Permiso negado.
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private  void SaveIntoSharedPreferences(String key, boolean value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    private boolean Update(String key){
        return prefs.getBoolean(key, false);

    }

}