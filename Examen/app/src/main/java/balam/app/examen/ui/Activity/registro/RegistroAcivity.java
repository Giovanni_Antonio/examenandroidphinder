package balam.app.examen.ui.Activity.registro;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.HashMap;
import java.util.Map;
import balam.app.examen.R;
import balam.app.examen.model.UserModel;
import balam.app.examen.tools.Common;
import balam.app.examen.ui.Activity.mainActivity.MainActivity;

public class RegistroAcivity  extends AppCompatActivity {

    Button registerUser;
    FirebaseFirestore mFirestore;
    DatabaseReference mDatabase;
    ConstraintLayout contentNewRegister;
    TextInputEditText editLatitud;
    TextInputEditText editLongitud;
    String latitud;
    String longitud;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_register_cliente);
       registerUser = findViewById(R.id.register_user);
        mFirestore = FirebaseFirestore.getInstance();
        contentNewRegister = findViewById(R.id.contentNewRegister);

       registerUser.setOnClickListener(view -> {
           TextInputEditText editRegistroId = findViewById(R.id.editId);
           TextInputEditText editTextNombre = findViewById(R.id.editTextNombre);
           editLatitud = findViewById(R.id.editLatitud);
           editLongitud = findViewById(R.id.editLongitud);

           String id = editRegistroId.getText().toString();
           String nombre = editTextNombre.getText().toString();
           latitud= editLatitud.getText().toString();
           longitud = editLongitud.getText().toString();

           Map<String, Object> map = new HashMap<>();
           map.put("id", id);
           map.put("nombre",nombre);
           map.put("latitud", latitud);
           map.put("longitud", longitud);

           mFirestore.collection(Common.COMMON_NEWUSER).add(map).addOnSuccessListener(documentReference -> {

               Toast.makeText(this, "¡Regístro con éxito en FireStore!", Toast.LENGTH_SHORT).show();
               Intent intent =new Intent(RegistroAcivity.this, MainActivity.class);
               startActivity(intent);


           }).addOnFailureListener(e ->
                   Snackbar.make(contentNewRegister,"Falló el registro.",Snackbar.LENGTH_SHORT).show());


       });
    }


}
