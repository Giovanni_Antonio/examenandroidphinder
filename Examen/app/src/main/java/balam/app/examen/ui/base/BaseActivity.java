package balam.app.examen.ui.base;

import android.content.Intent;
import android.graphics.Color;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;

import java.util.regex.Pattern;

public class BaseActivity extends AppCompatActivity {

    public void initView(int view){
        setContentView(view);
    }
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void showSnack(View view, String message) {
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        snackbar.show();
    }

    public void openIntent(Class<?> activity) {
        Intent intent = new Intent(this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void loadFragment(Fragment fragment, int resourceId) {
        getSupportFragmentManager().beginTransaction().replace(resourceId, fragment).commit();
    }

    public void loadFragment(Fragment fragment, int resourceId, String nameFragment) {
        getSupportFragmentManager().beginTransaction().replace(resourceId, fragment)
                .addToBackStack(nameFragment)
                .commit();
    }
    protected void showSnackbarShort(String message) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT).show();
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();

    }
}
